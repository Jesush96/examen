<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Pregunta;
use App\Alternativa;

class HomeController extends Controller
{

    public function ingresar()
    {
        $dni = Input::get('dni');
        return view('layouts/bienvenido',compact('dni'));
    }

    public function iniciar()
      {
          $dni = Input::get('dni');
          $preguntas=Pregunta::take(40)->get();
          for ($i=0; $i < sizeof($preguntas); $i++) {
            $alternativas[$i]=Alternativa::where('fk_pregunta',$preguntas[$i]->pk_pregunta)->get();

          }
          $opcion = array(
         "A) ",
         "B) ",
         "C)",

);
          //return $alternativas;
          return view('layouts/preguntaswait',compact('dni','preguntas','alternativas','opcion'));

      }

}
