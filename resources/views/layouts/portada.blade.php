@extends('app')

@section('content')

  <div class="account-pages"></div>
  <div class="clearfix"></div>
  <div class="wrapper-page">
         <div class="m-t-40 card-box">
             <div class="topbar-left">
                 <center>
                     <img src="{{ asset('img/logo2.png') }}" style="margin: 5px;height: 50px;" class="logo" alt="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label style="text-align:right" id="lblIp"></label>
                 </center>
             </div>

             <div class="text-center">
                 <a href="#"><h1><font color="#d4393d"> Examen de Conocimientos en la Conducción para Postulantes a Licencias de Conducir</font></h1></a>
             </div>
             <br>
             <div class="text-center" style="margin-bottom:1px">
                 <h4 class="text-uppercase font-bold m-b-0">Iniciar Sesión</h4>
             </div>
             <div class="panel-body">
                 <form class="form-horizontal m-t-10" method="Post" action="{{route('home.ingresar')}}">
                   {{csrf_field()}}
                     <div class="form-group">
                         <div class="col-md-6 col-md-offset-3" style="margin-bottom:10px">
                             <select class="form-control" id="listaTipoDoc" onchange="return cambioOpcionLista()">

                                 <option selected="selected" value="2">DNI</option>
                                 <option value="3">Pasaporte</option>
                                 <option value="4">CEX</option>
                                 <option value="5">Carné Solicitante</option>
                                 <option value="6">Pasaporte</option>
                                 <option value="13">Tarjeta de Identidad</option>
                                 <option value="14">PTP</option>

                             </select>
                         </div>
                         <div class="col-md-6 col-md-offset-3">
                             <input class="form-control" type="text" id="nroDoc" name="dni" placeholder="Ingrese su número de documento" align="middle" onkeypress="return limiteMayorCaracteres()" required>
                         </div>
                         <div id="mensajeUsuario" class="col-md-6 col-md-offset-3" style="display: none; margin-top: 5px; color: #d4393d">
                         </div>
                     </div>
                     <div class="form-group text-center m-t-30">
                         <div class="col-md-6 col-md-offset-3">
                             <input type="hidden" name="hdn1" id="hdn1" />
                             <input type="hidden" name="hdfuse" id="hdfuse" value="-" />
                             <input type="hidden" name="hdfhost" id="hdfhost" value="-" />
                             <input type="hidden" name="hdfmore" id="hdfmore" value="-" />
                             <button id="btnIngresar" class="btn btn-custom btn-bordred btn-block waves-effect waves-light"  type="submit">Ingresar</button>

                         </div>
                     </div>
                     <div class="form-group">
                         <div id="probando" class="col-md-6 col-md-offset-3" style="max-width:100%">
                             <div class="alert alert-danger" style="display: none; text-align:center" id="mensajeAviso">
                             </div>
                         </div>
                     </div>
                 </form>
             </div>
         </div>
         <!-- end card-box-->
         <div class="row">
             <div class="col-sm-12 text-center">
                 <p class="text-muted">
                     <strong>
                         <font color="#000000">Todos los derechos reservados © 2017 Ministerio de Transporte y Comunicaciones <br> Desarrollado por la <a href="http://www.mtc.gob.pe/transportes/terrestre/info_tramites.html" target="_blank" class="text-primary m-l-5"><b><font color="#000000">DGTT</font></b></a></font>
                     </strong>
                 </p>
             </div>
         </div>
     </div>




@endsection
