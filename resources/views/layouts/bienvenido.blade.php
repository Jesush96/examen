@extends('app')

@section('content')


    <!-- Navigation Bar-->
    <header id="topnav">
        <div class="topbar-main">
            <div class="container">
                <!-- LOGO -->
                <div class="topbar-left">
                    <img src="{{asset('img/logo2.png')}}" style="margin-top: 0px;height: 60px;" class="logo" alt="">
                </div>
                <!-- End Logo container-->
                <div class="menu-extras">
                    <ul class="nav navbar-nav navbar-right pull-right">
                        <li>
                            <!-- Notification -->
                            <div class="notification-box">
                                <ul class="list-inline m-b-0">
                                    <li>
                                        <a href="javascript:void(0);" class="right-bar-toggle">
                                            Bienvenido : {{$dni}}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- End Notification bar -->
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="navbar-custom">
            <div class="container">
                <div id="navigation">
                    <!-- Navigation Menu-->
                    <ul class="navigation-menu">
                        <li>
                            <a href="/examendrtc/public"><i class="zmdi zmdi-view-dashboard"></i> <span> Inicio </span> </a>
                        </li>
                    </ul>
                    <!-- End navigation menu-->
                </div>
            </div>
        </div>
    </header>
    <!-- End Navigation Bar-->


    <div class="wrapper">
        <div class="container">

            <br>

            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="text-center">

                                    <!--<h3 class="font-600">Antes de comenzar, seria bueno que sepas esto...</h3>
                                     <p class="text-muted"> Nisi praesentium similique totam odio obcaecati, reprehenderit,
                                        dignissimos rem temporibus ea inventore alias!<br/> Beatae animi nemo ea
                                        tempora, temporibus laborum facilis ut!</p> -->

                                </div>
                            </div><!-- end col -->
                        </div><!-- end row -->

                        <div class="row m-t-40">
                            <div class="col-md-8 col-md-offset-2">
                                <h3 class="font-600" align="center">EXAMEN DE CONOCIMIENTOS EN LA CONDUCCIÓN PARA POSTULANTES A LICENCIAS DE CONDUCIR </h3>
                                <br />
                                <h3 style="text-align:justify">Estimados Usuarios:</h3>
                                <br />
                                <h3 style="text-align:justify">Ante todo felicitarlos por ingresar a nuestro simulador de entrenamiento. El examen contiene 40 preguntas y tiene una duración de 40 minutos, y para su aprobación, el postulante deberá de acertar por lo menos treinta y cinco (35) respuestas de las cuarenta (40) preguntas. Es importante realizar este examen en completo silencio para evitar distracciones.</h3>

                                <h3 style="text-align:justify">Los componentes de este examen son temas de: Obligaciones del Conductor en materia de tránsito, Inspección Técnica Vehicular, Reglamento Nacional de Vehículos, Reglamento Nacional de Responsabilidad Civil y Seguros Obligatorios de Accidentes de Tránsito, Reglamento de Placa Única Nacional de Rodaje y Primeros Auxilios, en caso de accidentes de tránsito. </h3>

                                <h3 style="text-align:justify">Acabada la prueba, deseamos que lean los comentarios de todos e identifiquen el tema que les falta reforzar, con miras a estar aptos para el examen de Licencia de Conducir.</h3>

                                <h3 style="text-align:justify">El examen estará activo las 24 horas para que puedan resolver la prueba de conocimientos, según la categoría a la que postulan. Acabada la prueba, los resultados se podrán visualizar.</h3>

                                <h3 style="text-align:justify">Selecciona la categoría a la que postula: </h3>
                                <br />
                            </div>
                        </div><!-- end row -->
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">

                                <div class="text-center">

                                    <!-- <h3 class="font-600">...</h3> -->

                                    <center>
                                        <button type="button" name="button" class="btn btn-primary waves-effect waves-light m-t-10"
                                        data-toggle="modal" data-target="#myModal2">Iniciar</button>
                                    </center>

                                </div>
                            </div><!-- end col -->
                        </div><!-- end row -->


                    </div>
                </div><!-- end col -->
            </div>
            <!-- end row -->
            <!-- Footer -->
            <footer class="footer text-right">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-6">
                            Todos los derechos reservados © 2017-2018 MTC - Desarrollado por la DGTT.
                        </div>
                        <div class="col-xs-6">
                            <ul class="pull-right list-inline m-b-0">
                                <li>
                                    <a href="http://www.mtc.gob.pe/transportes/terrestre/info_tramites.html" target="_blank">Nosotros</a>
                                </li>
                                <!-- <li>
                                    <a href="#">Inscripciones</a>
                                </li> -->
                                <li>
                                    <a href="http://www.mtc.gob.pe/transportes/terrestre/contacto.html" target="_blank">Contacto</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- End Footer -->

        </div>
        <!-- end container -->

    </div>


    <!-- Modal -->
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document"   style="max-height:10%;max-width:60%;">
            <div class="modal-content">
                <!-- <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><font color="#E21C1C"><strong>¡ AVISO !</strong></font> </h4>
                </div> -->
                <div class="modal-body">
                    <div class="icon info" style="display: block;"></div>

                    <h2>Características del examen</h2>
                    <ul>
                        <li>Tiempo de duración: 40 minutos</li>
                        <li>Número de intentos: 1</li>
                        <li>Tiempo de apertura: 24/7</li>
                    </ul>
                    <p style="text-align: justify;">
                        * Una vez comenzado el simulacro permanecer en la página web.
                        <br />** No abrir o visualizar otras páginas mientras está desarrollando el simulacro, porque sino la plataforma lo detectará como inactividad y cerrará la sesión automáticamente.
                        <br />*** Tambien puede marcar las opciones con las teclas :
                        <!-- LOGO -->
                        <div class="topbar-left" style="text-align:center;">
                            <img src="{{asset('img/teclas.png')}}" style="margin-top: 0px;height: 60px; margin-left:140px;" class="logo" alt="">
                            <br />
                        </div><br /><br />
                        <!-- End Logo container-->
                        <br />Y avanzar presionando la tecla Enter :
                        <!-- LOGO -->
                        <div class="topbar-left" style="text-align:center;">
                            <img src="{{asset('img/enter.png')}}" style="margin-top: 10px;height: 100px; margin-left:210px;" class="logo" alt="">
                            <br />
                        </div><br /><br /><br /><br /><br />
                        <!-- End Logo container-->
                    </p>
                    <div class="alert alert-info">Asegúrese de tener una buena conexión a internet y no ser interrumpido.</div>
                    <h3 id="titleModalSimu">¿ Está seguro que desea comenzar el examen?</h3>
                </div>
                <form id="formGoSimu" action="{{route('home.iniciar')}}" method="post">
                  {{csrf_field()}}
                    <div class="modal-footer">
                        <input type="text" name="dni" value="{{$dni}}" hidden>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button id="goSimuladorModal" type="submit" class="btn btn-primary">Empezar  Examen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



@endsection
