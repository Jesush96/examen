@extends('app')

@section('content')


    <!-- Navigation Bar-->
    <header id="topnav">
        <div class="topbar-main">
            <div class="container">
                <!-- LOGO -->
                <div class="topbar-left">
                    <img src="{{asset('img/logo2.png')}}" style="margin-top: 0px;height: 60px;" class="logo" alt="">
                </div>
                <!-- End Logo container-->
                <div class="menu-extras">
                    <ul class="nav navbar-nav navbar-right pull-right">
                        <li>
                            <!-- Notification -->
                            <div class="notification-box">
                                <ul class="list-inline m-b-0">
                                    <li>
                                        <a href="javascript:void(0);" class="right-bar-toggle">
                                            Bienvenido : {{$dni}}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- End Notification bar -->
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="navbar-custom">
            <div class="container">
                <div id="navigation">
                    <!-- Navigation Menu-->
                    <ul class="navigation-menu">
                        <li>
                            <a href="/examendrtc/public"><i class="zmdi zmdi-view-dashboard"></i> <span> Inicio </span> </a>
                        </li>
                    </ul>
                    <!-- End navigation menu-->
                </div>
                <center>
                  <table class="countdownContainer" id="#countdown">
                    Tiempo
              			<tr class="info">
                      <td>00:</td>
              				<td id="minutes"></td>
                      <td>:</td>
              				<td id="seconds"></td>
              			</tr>
              		</table>

                </center>
            </div>
        </div>
    </header>
    <!-- End Navigation Bar-->


    <div class="wrapper">
        <div class="container">
          <!-- Page-Title -->
          <div class="row">
              <div class="col-sm-12">
                  <center><h3 class="page">Evaluaci&#243;n de conocimientos a postulantes a licencias de conducir de categor&#237;a</h3></center>
                  <center><h3 class="page">profesionales Clase A III-C (Vehiculos de la categor&#237;a M3 y N3)</h3></center>
              </div>
          </div>
          <br>

          <!--FORMULARIO PREGUNTA -->
          <form name="formularioSimulacro" action="/Resultados" method="post" onsubmit="return validateForm()">
              <input type="hidden" name="cantpreguntasTotal" id="cantpreguntasTotal" value="40">
              <input type="hidden" name="countSimuAux" id="countSimuAux" value="1">

              @for($i=0;$i < sizeof($preguntas);$i++)

              <div class="row">

                  <div class="col-md-8 col-md-offset-2" id="containerPregunta1">
                      <div class="card-box">
                          <h4 class="header-title m-t-0">Tema : Reglamento de Tr&#225;nsito y Manual de Dispositivos de Control de Tr&#225;nsito</h4>
                          <br>
                          <input type="hidden" name="idTema1" value="14">

                          <p class="text-muted font-15 m-b-15">
                              <strong>{{$i+1}}.- </strong>{{$preguntas[$i]->pregunta_texto}}
                          </p>
                          <!--ALTERNATIVAS-->
                          <div class="row">
                              <input type="hidden" name="dataDetected1" value="1332">
                              <div class="radio radio-danger" style="margin-top:5px">
                                @for($j=0;$j < 3;$j++)
                                <div class="radio">
                                  <input type="radio" name="radio1" id="radio1-1332" onchange="checkNext();" value="A">
                                     <label for="radio1-1332">
                                     {{$opcion[$j]}} {{$alternativas[$i][$j]->alternativa_texto}}<br>
                                    </label>
                                </div>
                                @endfor
                                <div class="radio">
                                  <input type="radio" name="radio1" id="radio1-1332" onchange="checkNext();" value="A">
                                     <label for="radio1-1332">
                                     D) Ninguna de las anteriores
                                    </label>
                                </div>
                              </div>
                          </div>
                          <!--END ALTERNATIVAS -->
                      </div>
                  </div><!-- end col -->

              </div>

                <!-- BARRA CARGA-->
                <div class="progress" style="height:20px;">
                  <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 5%;height:20px;"></div>
                </div>
                <!-- END BARRA CARGA-->


                <!-- BOTON SIGUIENTE-->
                <div class="container">
                    <center>
                        <button onclick="nextPregunta()"  id="btnSiguientePregunta" class="btn btn-primary m-b-5"> <span>Siguiente  &nbsp<i class="fa fa-arrow-right"></i> </span> </button>
                        <button style="display:none" id="btnFinishPregunta" type="button" class="btn btn-danger waves-effect waves-light m-b-5"> <i class="fa fa-check m-r-5"></i> <span>Simulacro Completo</span> </button>
                    </center>
                </div>
                <!--END BOTON SIGUIENTE-->

              @endfor


          </form>
          <!--END FORMULARIO PREGUNTA -->
        </div>
    </div>


@endsection
