@extends('app')

@section('content')
    <!-- Navigation Bar-->
    <header id="topnav">
        <div class="topbar-main">
            <div class="container">

                <!-- LOGO -->
                <div class="topbar-left">
                    <img src="{{asset('img/logo2.png')}}" style="margin-top: 0px;height: 60px;" class="logo" alt="">
                </div>
                <!-- End Logo container-->


                <div class="menu-extras">

                    <ul class="nav navbar-nav navbar-right pull-right">
                        <li>
                          <!-- Notification -->
                          <div class="notification-box">
                              <ul class="list-inline m-b-0">
                                  <li>
                                      <a href="javascript:void(0);" class="right-bar-toggle">
                                          Bienvenido : {{$dni}}
                                      </a>
                                  </li>
                              </ul>
                          </div>
                          <!-- End Notification bar -->
                        </li>
                    </ul>
                </div>

            </div>
        </div>
        <div class="navbar-custom">
            <div class="container">
                <div id="navigation">
                    <!-- Navigation Menu-->
                    <ul class="navigation-menu">
                        <li class="last-elements">
                            <a href="/examendrtc/public"><i class="zmdi zmdi-view-dashboard"></i> <span> Inicio </span> </a>
                        </li>
                    </ul>
                    <!-- End navigation menu  -->
                </div>
                <center> <div id="clock"><span class="titleContandorSimulacro">Tiempo Restante :</span> <span class="contadorSimulacro">00:39:45</span>
                  <script>
                          //1800000
                          var fiveSeconds = new Date().getTime() + 2400000;
                          $('#clock').countdown(fiveSeconds)
                          .on('update.countdown', function(event) {
                            var $this = $(this);
                            //console.log($this);
                            if (event.elapsed) {
                              //timeOutSimu();
                              //return false;
                              //alert("se termino su tiempo !!!");
                            } else {
                              $this.html(event.strftime('<span class="titleContandorSimulacro">Tiempo Restante :</span> <span class="contadorSimulacro">%H:%M:%S</span>'));
                              var dataCount =$('.contadorSimulacro').text();
                              var arrayCount = dataCount.split(":");
                              if (arrayCount[0]=="00") {
                                  if (arrayCount[1]=="00") {
                                      if (arrayCount[2]=="01") {
                                          timeOutSimu();
                                      }
                                  }
                              }
                            }
                          });

                          $("#myModal2").on('hidden.bs.modal', function () {
                              $("#checkout2").val(1);
                          });
                  </script>
                </div></center>
            </div>
        </div>
    </header>
    <!-- End Navigation Bar-->

    <div class="wrapper">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <center><h3 class="page">Evaluación de conocimientos en la conducción para postulantes</h3></center>
                    <center><h3 class="page">a licencias de conducir de clase AI(Vehículos de la categoría M1, M2 y N1)</h3></center>
                </div>
            </div>
            <br>
            <form name="formularioSimulacro" action="https://sierdgtt.mtc.gob.pe/Resultados" method="post" onsubmit="return validateForm()">
                <input type="hidden" name="cantpreguntasTotal" id="cantpreguntasTotal" value="40">
                <input type="hidden" name="countSimuAux" id="countSimuAux" value="1">


                <div class="row">
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta1">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema1" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>1.-</strong> Si durante la conducción vehicular, un efectivo de la Policía de Tránsito le solicita al conductor someterse a una prueba de alcoholemia; la acción correcta del conductor es:
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected1" value="94">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio1" id="radio1-94" onchange="checkNext();" value="A">
   <label for="radio1-94">
                                       A) Someterse a la prueba de alcoholemia, ya que está obligado a ello ante la solicitud del efectivo de la Policía de Tránsito.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio1" id="radio2-94" onchange="checkNext();" value="B">
   <label for="radio2-94">
                                      B)  Someterse o negarse a la prueba de alcoholemia, ya que no constituye una obligación del conductor realizarse dicha prueba.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio1" id="radio3-94" onchange="checkNext();" value="C">
   <label for="radio3-94">
                                        C) Negarse a la prueba de alcoholemia, ya que únicamente es exigible si ha participado en un accidente de tránsito.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio1" id="radio4-94" onchange="checkNext();" value="D">
   <label for="radio4-94">
                                            D) Ninguna de las alternativas es correcta.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta2" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema2" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>2.-</strong> ¿Qué debería hacer el conductor al acercarse a una señal de “CEDA EL
PASO” en una intersección?

                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected2" value="78">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio2" id="radio1-78" onchange="checkNext();" value="A">
   <label for="radio1-78">
                                       A) Ceder el paso a los vehículos de emergencia.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio2" id="radio2-78" onchange="checkNext();" value="B">
   <label for="radio2-78">
                                      B)  Mantener la velocidad y avanzar con cuidado.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio2" id="radio3-78" onchange="checkNext();" value="C">
   <label for="radio3-78">
                                        C) Disminuir la velocidad, parar si es necesario y ceder el paso a los peatones o vehículos que circulan por la vía transversal.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio2" id="radio4-78" onchange="checkNext();" value="D">
   <label for="radio4-78">
                                            D) Parar totalmente y luego avanzar con cuidado.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta3" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema3" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>3.-</strong> La siguiente señal vertical reglamentaria P-17A, indica:
                            </p>
                                <center> <img style="padding-top:15px;padding-bottom:15px;" src="./MTC _ Simulacro de Examen de Conocimientos_files/img50.jpg"> </center>
                            <div class="row">
                                <input type="hidden" name="dataDetected3" value="50">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio3" id="radio1-50" onchange="checkNext();" value="A">
   <label for="radio1-50">
                                       A) Reducción de la calzada al lado derecho.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio3" id="radio2-50" onchange="checkNext();" value="B">
   <label for="radio2-50">
                                      B)  Reducción de la calzada al lado izquierdo.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio3" id="radio3-50" onchange="checkNext();" value="C">
   <label for="radio3-50">
                                        C) Reducción de la calzada en ambos lados.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio3" id="radio4-50" onchange="checkNext();" value="D">
   <label for="radio4-50">
                                            D) Ensanchamiento de la calzada en ambos lados.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta4" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema4" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>4.-</strong> ¿Cuál es la sanción por conducir con presencia de alcohol en la sangre en proporción mayor a lo previsto en el Código Penal, o bajo los efectos de estupefacientes, narcóticos y/o alucinógenos comprobado con el examen respectivo, o por negarse al mismo y que haya participado en un accidente de tránsito?
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected4" value="60">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio4" id="radio1-60" onchange="checkNext();" value="A">
   <label for="radio1-60">
                                       A) Multa.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio4" id="radio2-60" onchange="checkNext();" value="B">
   <label for="radio2-60">
                                      B)  Cancelación de licencia de conducir.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio4" id="radio3-60" onchange="checkNext();" value="C">
   <label for="radio3-60">
                                        C) Suspensión de la licencia de conducir.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio4" id="radio4-60" onchange="checkNext();" value="D">
   <label for="radio4-60">
                                            D) Multa, cancelación de la licencia de conducir e inhabilitación definitiva para obtener una licencia de conducir.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta5" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema5" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>5.-</strong> Si una fila de escolares cruza la calzada fuera del crucero peatonal, ¿qué acción se debe tomar?
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected5" value="73">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio5" id="radio1-73" onchange="checkNext();" value="A">
   <label for="radio1-73">
                                       A) Advertir con el claxon.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio5" id="radio2-73" onchange="checkNext();" value="B">
   <label for="radio2-73">
                                      B)  Advertir a viva voz.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio5" id="radio3-73" onchange="checkNext();" value="C">
   <label for="radio3-73">
                                        C) Parar y ceder el paso.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio5" id="radio4-73" onchange="checkNext();" value="D">
   <label for="radio4-73">
                                            D) Continuar la marcha lentamente.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta6" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema6" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>6.-</strong> La siguiente señal (P-36), significa:
                            </p>
                                <center> <img style="padding-top:15px;padding-bottom:15px;" src="./MTC _ Simulacro de Examen de Conocimientos_files/img102.jpg"> </center>
                            <div class="row">
                                <input type="hidden" name="dataDetected6" value="102">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio6" id="radio1-102" onchange="checkNext();" value="A">
   <label for="radio1-102">
                                       A) Proximidad de un túnel.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio6" id="radio2-102" onchange="checkNext();" value="B">
   <label for="radio2-102">
                                      B)  Superficie deslizante.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio6" id="radio3-102" onchange="checkNext();" value="C">
   <label for="radio3-102">
                                        C) Prender las luces bajas.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio6" id="radio4-102" onchange="checkNext();" value="D">
   <label for="radio4-102">
                                            D) Mantener la distancia entre vehículos por seguridad.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta7" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema7" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>7.-</strong> Los semáforos son:
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected7" value="184">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio7" id="radio1-184" onchange="checkNext();" value="A">
   <label for="radio1-184">
                                       A) Dispositivos de control del tránsito que tienen por finalidad regular y controlar el tránsito vehicular, motorizado y no motorizado, y el peatonal, a través de las indicaciones de las luces respectivas.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio7" id="radio2-184" onchange="checkNext();" value="B">
   <label for="radio2-184">
                                      B)  Dispositivos de control del tránsito que tienen por finalidad regular y controlar únicamente el tránsito vehicular motorizado y peatonal, a través de las indicaciones de luces.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio7" id="radio3-184" onchange="checkNext();" value="C">
   <label for="radio3-184">
                                        C) Señales de color rojo, verde y amarillo, que tienen como único fin regular la corriente vehicular.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio7" id="radio4-184" onchange="checkNext();" value="D">
   <label for="radio4-184">
                                            D) Artefactos que emiten luces de colores y cuyo único fin es regular la corriente de vehículos motorizados y peatones.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta8" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema8" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>8.-</strong> La siguiente señal (P-48-B), indica:
                            </p>
                                <center> <img style="padding-top:15px;padding-bottom:15px;" src="./MTC _ Simulacro de Examen de Conocimientos_files/img125.jpg"> </center>
                            <div class="row">
                                <input type="hidden" name="dataDetected8" value="125">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio8" id="radio1-125" onchange="checkNext();" value="A">
   <label for="radio1-125">
                                       A) Ubicación de un cruce escolar.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio8" id="radio2-125" onchange="checkNext();" value="B">
   <label for="radio2-125">
                                      B)  Proximidad a una calzada.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio8" id="radio3-125" onchange="checkNext();" value="C">
   <label for="radio3-125">
                                        C) Ubicación de un cruce peatonal.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio8" id="radio4-125" onchange="checkNext();" value="D">
   <label for="radio4-125">
                                            D) Ninguna de las alternativas es correcta.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta9" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema9" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>9.-</strong> Son señales que regulan el tránsito:
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected9" value="192">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio9" id="radio1-192" onchange="checkNext();" value="A">
   <label for="radio1-192">
                                       A) Las bocinas y las marcas en la calzada o señales horizontales.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio9" id="radio2-192" onchange="checkNext();" value="B">
   <label for="radio2-192">
                                      B)  Las bocinas y las señales verticales.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio9" id="radio3-192" onchange="checkNext();" value="C">
   <label for="radio3-192">
                                        C) Las señales verticales, las marcas en la calzada o señales horizontales y las bocinas.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio9" id="radio4-192" onchange="checkNext();" value="D">
   <label for="radio4-192">
                                            D) Las  señales verticales  y  las  marcas  en  la  calzada  o  señales horizontales.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta10" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema10" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>10.-</strong> La siguiente señal vertical reglamentaria R-53:
                            </p>
                                <center> <img style="padding-top:15px;padding-bottom:15px;" src="./MTC _ Simulacro de Examen de Conocimientos_files/img9.jpg"> </center>
                            <div class="row">
                                <input type="hidden" name="dataDetected10" value="9">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio10" id="radio1-9" onchange="checkNext();" value="A">
   <label for="radio1-9">
                                       A) Prohíbe estacionar.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio10" id="radio2-9" onchange="checkNext();" value="B">
   <label for="radio2-9">
                                      B)  Prohíbe al conductor detener el vehículo dentro del área de la intersección.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio10" id="radio3-9" onchange="checkNext();" value="C">
   <label for="radio3-9">
                                        C) Prohíbe la carga y descarga.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio10" id="radio4-9" onchange="checkNext();" value="D">
   <label for="radio4-9">
                                            D) Prohíbe la circulación de vehículos motorizados
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta11" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema11" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>11.-</strong> La siguiente señal (P-49A), indica:
                            </p>
                                <center> <img style="padding-top:15px;padding-bottom:15px;" src="./MTC _ Simulacro de Examen de Conocimientos_files/img127.jpg"> </center>
                            <div class="row">
                                <input type="hidden" name="dataDetected11" value="127">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio11" id="radio1-127" onchange="checkNext();" value="A">
   <label for="radio1-127">
                                       A) Zona escolar.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio11" id="radio2-127" onchange="checkNext();" value="B">
   <label for="radio2-127">
                                      B)  Proximidad a un cruce escolar.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio11" id="radio3-127" onchange="checkNext();" value="C">
   <label for="radio3-127">
                                        C) Ubicación de un cruce escolar.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio11" id="radio4-127" onchange="checkNext();" value="D">
   <label for="radio4-127">
                                            D) Ninguna de las alternativas es correcta.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta12" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema12" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>12.-</strong> El Reglamento Nacional de Tránsito considera la reincidencia en las infracciones de tránsito?
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected12" value="174">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio12" id="radio1-174" onchange="checkNext();" value="A">
   <label for="radio1-174">
                                       A) No, el conductor puede cometer la misma infracción varias veces y la sanción será la misma.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio12" id="radio2-174" onchange="checkNext();" value="B">
   <label for="radio2-174">
                                      B)  Si, cuando el conductor comete la misma infracción dentro de los 12 meses será sancionado con el doble de la multa.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio12" id="radio3-174" onchange="checkNext();" value="C">
   <label for="radio3-174">
                                        C) Solo en los casos en que se cometan infracciones gravísimas.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio12" id="radio4-174" onchange="checkNext();" value="D">
   <label for="radio4-174">
                                            D) Ninguna de las alternativas es correcta.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta13" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema13" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>13.-</strong> Son documentos que deben portarse obligatoriamente, durante la conducción del vehículo, y exhibirse cuando la autoridad competente lo solicite:
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected13" value="40">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio13" id="radio1-40" onchange="checkNext();" value="A">
   <label for="radio1-40">
                                       A) Documento de identidad, SOAT vigente (puede ser virtual) y tarjeta de identificación vehicular.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio13" id="radio2-40" onchange="checkNext();" value="B">
   <label for="radio2-40">
                                      B)  Certificado de Inspección Técnica Vehicular y contrato de compraventa del vehículo.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio13" id="radio3-40" onchange="checkNext();" value="C">
   <label for="radio3-40">
                                        C) Contrato de compraventa del vehículo.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio13" id="radio4-40" onchange="checkNext();" value="D">
   <label for="radio4-40">
                                            D) Todas las alternativas son correctas.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta14" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema14" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>14.-</strong> La señal preventiva P-33A, significa:
                            </p>
                                <center> <img style="padding-top:15px;padding-bottom:15px;" src="./MTC _ Simulacro de Examen de Conocimientos_files/img67.jpg"> </center>
                            <div class="row">
                                <input type="hidden" name="dataDetected14" value="67">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio14" id="radio1-67" onchange="checkNext();" value="A">
   <label for="radio1-67">
                                       A) Señal de curva sinuosa.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio14" id="radio2-67" onchange="checkNext();" value="B">
   <label for="radio2-67">
                                      B)  Señal de proximidad a un badén.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio14" id="radio3-67" onchange="checkNext();" value="C">
   <label for="radio3-67">
                                        C) Señal de proximidad de un reductor de velocidad tipo resalto.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio14" id="radio4-67" onchange="checkNext();" value="D">
   <label for="radio4-67">
                                            D) Ninguna de las alternativas es correcta.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta15" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema15" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>15.-</strong> La siguiente señal (I-20), se utiliza para indicar:
                            </p>
                                <center> <img style="padding-top:15px;padding-bottom:15px;" src="./MTC _ Simulacro de Examen de Conocimientos_files/img147.jpg"> </center>
                            <div class="row">
                                <input type="hidden" name="dataDetected15" value="147">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio15" id="radio1-147" onchange="checkNext();" value="A">
   <label for="radio1-147">
                                       A) Cercanía a un servicio mecánico.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio15" id="radio2-147" onchange="checkNext();" value="B">
   <label for="radio2-147">
                                      B)  Cercanía a una zona donde debe circular con cadenas en las llantas.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio15" id="radio3-147" onchange="checkNext();" value="C">
   <label for="radio3-147">
                                        C) Cercanía a un grifo.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio15" id="radio4-147" onchange="checkNext();" value="D">
   <label for="radio4-147">
                                            D) Cercanía a una llantería.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta16" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema16" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>16.-</strong> Está permitido en la vía:
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected16" value="1">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio16" id="radio1-1" onchange="checkNext();" value="A">
   <label for="radio1-1">
                                       A) Recoger o dejar pasajeros o carga en cualquier lugar.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio16" id="radio2-1" onchange="checkNext();" value="B">
   <label for="radio2-1">
                                      B)  Dejar animales sueltos o situarlos de forma tal que obstaculicen solo un poco el tránsito.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio16" id="radio3-1" onchange="checkNext();" value="C">
   <label for="radio3-1">
                                        C) Recoger o dejar pasajeros en lugares autorizados
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio16" id="radio4-1" onchange="checkNext();" value="D">
   <label for="radio4-1">
                                            D) Ejercer el comercio ambulatorio o estacionario.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta17" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema17" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>17.-</strong> La siguiente señal (P-34), le advierte al conductor que:
                            </p>
                                <center> <img style="padding-top:15px;padding-bottom:15px;" src="./MTC _ Simulacro de Examen de Conocimientos_files/img118.jpg"> </center>
                            <div class="row">
                                <input type="hidden" name="dataDetected17" value="118">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio17" id="radio1-118" onchange="checkNext();" value="A">
   <label for="radio1-118">
                                       A) La vía está en mal estado y tiene baches.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio17" id="radio2-118" onchange="checkNext();" value="B">
   <label for="radio2-118">
                                      B)  Se aproxima a un resalto.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio17" id="radio3-118" onchange="checkNext();" value="C">
   <label for="radio3-118">
                                        C) Se aproxima a un rompe muelles.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio17" id="radio4-118" onchange="checkNext();" value="D">
   <label for="radio4-118">
                                            D) Se aproxima a un badén.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta18" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema18" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>18.-</strong> La siguiente señal (P-48A), indica:
                            </p>
                                <center> <img style="padding-top:15px;padding-bottom:15px;" src="./MTC _ Simulacro de Examen de Conocimientos_files/img124.jpg"> </center>
                            <div class="row">
                                <input type="hidden" name="dataDetected18" value="124">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio18" id="radio1-124" onchange="checkNext();" value="A">
   <label for="radio1-124">
                                       A) Vía es de uso exclusivo de peatones.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio18" id="radio2-124" onchange="checkNext();" value="B">
   <label for="radio2-124">
                                      B)  Proximidad a un cruce peatonal.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio18" id="radio3-124" onchange="checkNext();" value="C">
   <label for="radio3-124">
                                        C) Ubicación de un cruce escolar.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio18" id="radio4-124" onchange="checkNext();" value="D">
   <label for="radio4-124">
                                            D) Ninguna de las alternativas es correcta.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta19" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema19" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>19.-</strong> ¿Está permitido usar la bocina de su vehículo para advertir al conductor del vehículo que circula delante, que será adelantado?
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected19" value="69">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio19" id="radio1-69" onchange="checkNext();" value="A">
   <label for="radio1-69">
                                       A) Sí, siempre y cuando el sonido no sea estridente.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio19" id="radio2-69" onchange="checkNext();" value="B">
   <label for="radio2-69">
                                      B)  Si, salvo prohibición expresa mediante la correspondiente señal.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio19" id="radio3-69" onchange="checkNext();" value="C">
   <label for="radio3-69">
                                        C) No, está prohibido.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio19" id="radio4-69" onchange="checkNext();" value="D">
   <label for="radio4-69">
                                            D) Ninguna de las alternativas es correcta.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta20" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema20" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>20.-</strong> El siguiente gráfico muestra:
                            </p>
                                <center> <img style="padding-top:15px;padding-bottom:15px;" src="./MTC _ Simulacro de Examen de Conocimientos_files/img197.jpg"> </center>
                            <div class="row">
                                <input type="hidden" name="dataDetected20" value="197">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio20" id="radio1-197" onchange="checkNext();" value="A">
   <label for="radio1-197">
                                       A) Señalización de tránsito vertical y horizontal en una zona escolar.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio20" id="radio2-197" onchange="checkNext();" value="B">
   <label for="radio2-197">
                                      B)  Únicamente señalización de tránsito vertical en una zona escolar.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio20" id="radio3-197" onchange="checkNext();" value="C">
   <label for="radio3-197">
                                        C) Únicamente señalización de tránsito horizontal en una zona escolar.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio20" id="radio4-197" onchange="checkNext();" value="D">
   <label for="radio4-197">
                                            D) Ninguna de las alternativas es correcta.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta21" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema21" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>21.-</strong> A fin de determinar la presencia de alcohol o sustancias estupefacientes en el conductor, el efectivo de la Policía de Tránsito puede exigirle al momento de la intervención:
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected21" value="173">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio21" id="radio1-173" onchange="checkNext();" value="A">
   <label for="radio1-173">
                                       A) Que realice pruebas de coordinación y equilibrio.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio21" id="radio2-173" onchange="checkNext();" value="B">
   <label for="radio2-173">
                                      B)  Uso del alcoholímetro.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio21" id="radio3-173" onchange="checkNext();" value="C">
   <label for="radio3-173">
                                        C) Prueba de alcoholemia.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio21" id="radio4-173" onchange="checkNext();" value="D">
   <label for="radio4-173">
                                            D) Todas las alternativas son correctas.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta22" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema22" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>22.-</strong> La siguiente señal (R-40), significa:
                            </p>
                                <center> <img style="padding-top:15px;padding-bottom:15px;" src="./MTC _ Simulacro de Examen de Conocimientos_files/img110.jpg"> </center>
                            <div class="row">
                                <input type="hidden" name="dataDetected22" value="110">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio22" id="radio1-110" onchange="checkNext();" value="A">
   <label for="radio1-110">
                                       A) Crucero tipo cebra.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio22" id="radio2-110" onchange="checkNext();" value="B">
   <label for="radio2-110">
                                      B)  Control policial.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio22" id="radio3-110" onchange="checkNext();" value="C">
   <label for="radio3-110">
                                        C) Pavimento deslizante.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio22" id="radio4-110" onchange="checkNext();" value="D">
   <label for="radio4-110">
                                            D) Circular con luces bajas.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta23" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema23" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>23.-</strong> En una rotonda, tiene prioridad de paso el vehículo que:
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected23" value="37">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio23" id="radio1-37" onchange="checkNext();" value="A">
   <label for="radio1-37">
                                       A) Quiere ingresar a la rotonda.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio23" id="radio2-37" onchange="checkNext();" value="B">
   <label for="radio2-37">
                                      B)  Circula por ella.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio23" id="radio3-37" onchange="checkNext();" value="C">
   <label for="radio3-37">
                                        C) Acelera primero.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio23" id="radio4-37" onchange="checkNext();" value="D">
   <label for="radio4-37">
                                            D) Hace sonar la bocina.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta24" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema24" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>24.-</strong> Es una parte de la vía destinada al uso de peatones
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected24" value="191">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio24" id="radio1-191" onchange="checkNext();" value="A">
   <label for="radio1-191">
                                       A) La acera.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio24" id="radio2-191" onchange="checkNext();" value="B">
   <label for="radio2-191">
                                      B)  La calzada.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio24" id="radio3-191" onchange="checkNext();" value="C">
   <label for="radio3-191">
                                        C) La berma.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio24" id="radio4-191" onchange="checkNext();" value="D">
   <label for="radio4-191">
                                            D) La autopista.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta25" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema25" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>25.-</strong> La siguiente señal (P-61), le advierte al conductor que:
                            </p>
                                <center> <img style="padding-top:15px;padding-bottom:15px;" src="./MTC _ Simulacro de Examen de Conocimientos_files/img117.jpg"> </center>
                            <div class="row">
                                <input type="hidden" name="dataDetected25" value="117">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio25" id="radio1-117" onchange="checkNext();" value="A">
   <label for="radio1-117">
                                       A) El sentido del tránsito es el que indica la flecha.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio25" id="radio2-117" onchange="checkNext();" value="B">
   <label for="radio2-117">
                                      B)  Se aproxima a una reducción de la vía en ambos sentidos.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio25" id="radio3-117" onchange="checkNext();" value="C">
   <label for="radio3-117">
                                        C) Está circulando por una curva horizontal.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio25" id="radio4-117" onchange="checkNext();" value="D">
   <label for="radio4-117">
                                            D) Ninguna de las alternativas es correcta.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta26" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema26" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>26.-</strong> La siguiente señal (R-48), significa:
                            </p>
                                <center> <img style="padding-top:15px;padding-bottom:15px;" src="./MTC _ Simulacro de Examen de Conocimientos_files/img111.jpg"> </center>
                            <div class="row">
                                <input type="hidden" name="dataDetected26" value="111">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio26" id="radio1-111" onchange="checkNext();" value="A">
   <label for="radio1-111">
                                       A) Los peatones deben circular por la derecha y los camiones por la izquierda.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio26" id="radio2-111" onchange="checkNext();" value="B">
   <label for="radio2-111">
                                      B)  Los peatones deben tener cuidado con los camiones.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio26" id="radio3-111" onchange="checkNext();" value="C">
   <label for="radio3-111">
                                        C) Zona de carga y descarga.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio26" id="radio4-111" onchange="checkNext();" value="D">
   <label for="radio4-111">
                                            D) Ninguna de las alternativas es correcta.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta27" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema27" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>27.-</strong> La siguiente señal (I-18), se utiliza para indicar:
                            </p>
                                <center> <img style="padding-top:15px;padding-bottom:15px;" src="./MTC _ Simulacro de Examen de Conocimientos_files/img145.jpg"> </center>
                            <div class="row">
                                <input type="hidden" name="dataDetected27" value="145">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio27" id="radio1-145" onchange="checkNext();" value="A">
   <label for="radio1-145">
                                       A) Cercanía a una ferretería.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio27" id="radio2-145" onchange="checkNext();" value="B">
   <label for="radio2-145">
                                      B)  Cercanía a un servicio mecánico.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio27" id="radio3-145" onchange="checkNext();" value="C">
   <label for="radio3-145">
                                        C) Cercanía a un grifo.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio27" id="radio4-145" onchange="checkNext();" value="D">
   <label for="radio4-145">
                                            D) Ninguna de las alternativas es correcta.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta28" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema28" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>28.-</strong> La siguiente señal (R-30C), significa:
                            </p>
                                <center> <img style="padding-top:15px;padding-bottom:15px;" src="./MTC _ Simulacro de Examen de Conocimientos_files/img105.jpg"> </center>
                            <div class="row">
                                <input type="hidden" name="dataDetected28" value="105">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio28" id="radio1-105" onchange="checkNext();" value="A">
   <label for="radio1-105">
                                       A) Que la velocidad máxima de la vía es de 50 km/h.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio28" id="radio2-105" onchange="checkNext();" value="B">
   <label for="radio2-105">
                                      B)  Que la velocidad mínima de la vía es de 50 km/h.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio28" id="radio3-105" onchange="checkNext();" value="C">
   <label for="radio3-105">
                                        C) Que al salir de la vía por donde está circulando, la velocidad máxima es 50 km/h.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio28" id="radio4-105" onchange="checkNext();" value="D">
   <label for="radio4-105">
                                            D) Que al salir de la vía por donde está circulando, la velocidad mínima es 50 km/h.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta29" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema29" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>29.-</strong> Al aproximarse a una intersección con giro permitido a la izquierda, la conducta correcta es:
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected29" value="13">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio29" id="radio1-13" onchange="checkNext();" value="A">
   <label for="radio1-13">
                                       A) Girar desde cualquier carril.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio29" id="radio2-13" onchange="checkNext();" value="B">
   <label for="radio2-13">
                                      B)  Colocarse en el carril derecho, luego girar con precaución.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio29" id="radio3-13" onchange="checkNext();" value="C">
   <label for="radio3-13">
                                        C) Colocarse en el carril más despejado de tráfico, luego girar con precaución.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio29" id="radio4-13" onchange="checkNext();" value="D">
   <label for="radio4-13">
                                            D) Hacer la señal de volteo a la izquierda con las luces direccionales, ubicar con antelación el vehículo en el carril de circulación de la izquierda y girar con precaución.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta30" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema30" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>30.-</strong> Se entiende por carril a la:
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected30" value="48">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio30" id="radio1-48" onchange="checkNext();" value="A">
   <label for="radio1-48">
                                       A) Parte de la vía destinada a la circulación de peatones.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio30" id="radio2-48" onchange="checkNext();" value="B">
   <label for="radio2-48">
                                      B)  Parte de la calzada destinada al tránsito de una fila de vehículos.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio30" id="radio3-48" onchange="checkNext();" value="C">
   <label for="radio3-48">
                                        C) Vía rural destinada a la circulación de peatones y animales.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio30" id="radio4-48" onchange="checkNext();" value="D">
   <label for="radio4-48">
                                            D) Todas las alternativas son correctas.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta31" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema31" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>31.-</strong> El uso de la luz ___________ es obligatorio en __________ , debiendo cambiar por luz ____________ momentos previos al cruce con otro vehículo que circule en sentido contrario.
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected31" value="162">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio31" id="radio1-162" onchange="checkNext();" value="A">
   <label for="radio1-162">
                                       A) Baja - carreteras - alta.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio31" id="radio2-162" onchange="checkNext();" value="B">
   <label for="radio2-162">
                                      B)  Alta - vías urbanas - baja.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio31" id="radio3-162" onchange="checkNext();" value="C">
   <label for="radio3-162">
                                        C) Rompenieblas - carreteras - baja.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio31" id="radio4-162" onchange="checkNext();" value="D">
   <label for="radio4-162">
                                            D) Alta - carretereras - baja.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta32" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema32" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>32.-</strong> ¿Después de qué tiempo de haber cometido la misma infracción se llama reincidencia y es sancionada con el doble de la multa establecida?
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected32" value="175">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio32" id="radio1-175" onchange="checkNext();" value="A">
   <label for="radio1-175">
                                       A) 06 meses.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio32" id="radio2-175" onchange="checkNext();" value="B">
   <label for="radio2-175">
                                      B)  12 meses.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio32" id="radio3-175" onchange="checkNext();" value="C">
   <label for="radio3-175">
                                        C) 24 meses.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio32" id="radio4-175" onchange="checkNext();" value="D">
   <label for="radio4-175">
                                            D) 35 meses.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta33" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema33" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>33.-</strong> Una persona con discapacidad física puede obtener una licencia de conducir particular?
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected33" value="156">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio33" id="radio1-156" onchange="checkNext();" value="A">
   <label for="radio1-156">
                                       A) Sí, siempre y cuando dicha discapacidad pueda ser superada con algún corrector que establezca alguna de las restricciones que prevé la norma vigente.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio33" id="radio2-156" onchange="checkNext();" value="B">
   <label for="radio2-156">
                                      B)  No, está terminantemente prohibido.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio33" id="radio3-156" onchange="checkNext();" value="C">
   <label for="radio3-156">
                                        C) Depende del criterio del centro de emisión de la licencia de conducir.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio33" id="radio4-156" onchange="checkNext();" value="D">
   <label for="radio4-156">
                                            D) Ninguna de las alternativas es correcta.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta34" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema34" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>34.-</strong> Si se aproxima a una zona escolar, ¿que acción debe realizar?
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected34" value="74">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio34" id="radio1-74" onchange="checkNext();" value="A">
   <label for="radio1-74">
                                       A) No tiene ninguna obligación si no hay señalización.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio34" id="radio2-74" onchange="checkNext();" value="B">
   <label for="radio2-74">
                                      B)  Disminuir la velocidad a 40 Km/h.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio34" id="radio3-74" onchange="checkNext();" value="C">
   <label for="radio3-74">
                                        C) Disminuir la velocidad a 30 Km/h.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio34" id="radio4-74" onchange="checkNext();" value="D">
   <label for="radio4-74">
                                            D) Disminuir la velocidad a 35 km/h.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta35" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema35" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>35.-</strong> La siguiente señal (P-46B), indica:
                            </p>
                                <center> <img style="padding-top:15px;padding-bottom:15px;" src="./MTC _ Simulacro de Examen de Conocimientos_files/img122.jpg"> </center>
                            <div class="row">
                                <input type="hidden" name="dataDetected35" value="122">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio35" id="radio1-122" onchange="checkNext();" value="A">
   <label for="radio1-122">
                                       A) Que los ciclistas deben detenerse en ese punto.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio35" id="radio2-122" onchange="checkNext();" value="B">
   <label for="radio2-122">
                                      B)  Que nos aproximamos a un cruce de ciclovía.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio35" id="radio3-122" onchange="checkNext();" value="C">
   <label for="radio3-122">
                                        C) Que la ciclovía es solo para los ciclistas.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio35" id="radio4-122" onchange="checkNext();" value="D">
   <label for="radio4-122">
                                            D) La ubicación de un cruce de ciclistas.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta36" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema36" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>36.-</strong> Indique la conducta permitida:
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected36" value="163">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio36" id="radio1-163" onchange="checkNext();" value="A">
   <label for="radio1-163">
                                       A) El estacionamiento de un vehículo a la salida de salas de espectáculos en funcionamiento.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio36" id="radio2-163" onchange="checkNext();" value="B">
   <label for="radio2-163">
                                      B)  El estacionamiento de un vehículo de emergencia en un lugar no permitido, si ello fuera imprescindible.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio36" id="radio3-163" onchange="checkNext();" value="C">
   <label for="radio3-163">
                                        C) El estacionamiento de un vehículo después de 1 metro de un paso peatonal.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio36" id="radio4-163" onchange="checkNext();" value="D">
   <label for="radio4-163">
                                            D) El estacionamiento de un vehículo sobre las aceras.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta37" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema37" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>37.-</strong> ¿Cuál es el plazo de vigencia del SOAT?
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected37" value="64">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio37" id="radio1-64" onchange="checkNext();" value="A">
   <label for="radio1-64">
                                       A) 1 año.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio37" id="radio2-64" onchange="checkNext();" value="B">
   <label for="radio2-64">
                                      B)  6 meses.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio37" id="radio3-64" onchange="checkNext();" value="C">
   <label for="radio3-64">
                                        C) 4 años.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio37" id="radio4-64" onchange="checkNext();" value="D">
   <label for="radio4-64">
                                            D) 2 años.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta38" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema38" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>38.-</strong> La siguiente señal (P-55), indica:
                            </p>
                                <center> <img style="padding-top:15px;padding-bottom:15px;" src="./MTC _ Simulacro de Examen de Conocimientos_files/img132.jpg"> </center>
                            <div class="row">
                                <input type="hidden" name="dataDetected38" value="132">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio38" id="radio1-132" onchange="checkNext();" value="A">
   <label for="radio1-132">
                                       A) Semáforo malogrado.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio38" id="radio2-132" onchange="checkNext();" value="B">
   <label for="radio2-132">
                                      B)  Proximidad a un semáforo.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio38" id="radio3-132" onchange="checkNext();" value="C">
   <label for="radio3-132">
                                        C) Semáforos en ola verde.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio38" id="radio4-132" onchange="checkNext();" value="D">
   <label for="radio4-132">
                                            D) Ninguna de las alternativas es correcta.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta39" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema39" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>39.-</strong> Si llega a una intersección y visualiza el semáforo con una flecha roja hacia la izquierda y la luz circular verde prendidas al mismo tiempo, la acción correcta es:
                            </p>
                            <div class="row">
                                <input type="hidden" name="dataDetected39" value="10">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio39" id="radio1-10" onchange="checkNext();" value="A">
   <label for="radio1-10">
                                       A) Avanzar en cualquier sentido, ya que la luz circular está en verde.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio39" id="radio2-10" onchange="checkNext();" value="B">
   <label for="radio2-10">
                                      B)  Avanzar, pero el giro a la izquierda está prohibido por la flecha roja.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio39" id="radio3-10" onchange="checkNext();" value="C">
   <label for="radio3-10">
                                        C) Avanzar  únicamente  hacia  la  izquierda,  pues  continuar  de  frente  está prohibido.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio39" id="radio4-10" onchange="checkNext();" value="D">
   <label for="radio4-10">
                                            D) Ninguna de las alternativas es correcta.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                    <div class="col-md-8 col-md-offset-2" id="containerPregunta40" style="display: none;">
                        <div class="card-box">
                            <h4 class="header-title m-t-0">Tema : Reglamento de Tránsito y Manual de Dispositivos de Control de Tránsito</h4>
                            <br>
                            <input type="hidden" name="idTema40" value="14">

                            <p class="text-muted font-15 m-b-15">
                                <strong>40.-</strong> La siguiente señal (P-60), es:
                            </p>
                                <center> <img style="padding-top:15px;padding-bottom:15px;" src="./MTC _ Simulacro de Examen de Conocimientos_files/img119.jpg"> </center>
                            <div class="row">
                                <input type="hidden" name="dataDetected40" value="119">
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio40" id="radio1-119" onchange="checkNext();" value="A">
   <label for="radio1-119">
                                       A) Una señal turística.
                                        </label>
                                </div>
                                <!-- Segunda opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio40" id="radio2-119" onchange="checkNext();" value="B">
   <label for="radio2-119">
                                      B)  Una señal informativa.
                                    </label>
                                </div>
                                <!-- Tercera opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio40" id="radio3-119" onchange="checkNext();" value="C">
   <label for="radio3-119">
                                        C) Una señal preventiva.
                                    </label>
                                </div>
                                <!-- Cuarta opcion  -->
                                <div class="radio radio-danger" style="margin-top:5px">
                                    <input type="radio" name="radio40" id="radio4-119" onchange="checkNext();" value="D">
   <label for="radio4-119">
                                            D) Ninguna de las alternativas es correcta.
                                    </label>
                                </div>
                            </div>
                            <!-- end row -->

                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->
                <div class="col-md-10 col-md-offset-1" id="contentBarIdLoading">
                    <div class="progress progress-lg">
                        <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width: 0%;">
                            <span class="sr-only">0% Complete</span>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <center>
                        <button onclick="nextPregunta()" disabled="true" id="btnSiguientePregunta" class="btn btn-primary m-b-5"> <span>Siguiente  &nbsp;<i class="fa fa-arrow-right"></i> </span> </button>
                        <button style="display:none" id="btnFinishPregunta" type="button" class="btn btn-danger waves-effect waves-light m-b-5"> <i class="fa fa-check m-r-5"></i> <span>Examen Completo</span> </button>
                    </center>
                </div>

            </form>
            <br>
            <input type="hidden" id="positionPregunta" name="positionPregunta" value="1">
            <input type="hidden" name="checkout2" id="checkout2" value="1">
            <!-- Footer -->
            <footer class="footer text-right">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-6">
                            Todos los derechos reservados © 2017-2018 MTC - Desarrollado por la DGTT.
                        </div>
                        <div class="col-xs-6">
                            <ul class="pull-right list-inline m-b-0">
                                <li>
                                    <a href="http://www.mtc.gob.pe/transportes/terrestre/info_tramites.html" target="_blank">Nosotros</a>
                                </li>
                                <!-- <li>
                                    <a href="#">Inscripciones</a>
                                </li> -->
                                <li>
                                    <a href="http://www.mtc.gob.pe/transportes/terrestre/contacto.html" target="_blank">Contacto</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- End Footer -->

        </div>
        <!-- end container -->

    </div>






    <script>

        var cantpreguntasTotal = parseInt($("#cantpreguntasTotal").val());
        var inicio = $("#countSimuAux").val();
        for (var i = inicio; i < cantpreguntasTotal + 1 ; i++) {
            //for (var i = 1; i < 41; i++) {
            var idContainer = "#containerPregunta" + i;
            //alert(idContainer);
            if (i != inicio) {
                $(idContainer).hide();
            }
        }


        function checkNext() {

            $("#btnSiguientePregunta").attr("disabled", false);
            var preguntaActual = parseInt($("#positionPregunta").val());
           // alert(preguntaActual);
            //alert(preguntaActual);
            if (preguntaActual == cantpreguntasTotal) {

            //if (preguntaActual == 40) {
                $("#btnSiguientePregunta").hide();
                $("#btnFinishPregunta").show();
            }
        }

        $("#btnFinishPregunta").click(function () {
            $('#myModal2').modal('show');
            $("#checkout2").val(2);
        });



        function nextPregunta() {

            $("#btnSiguientePregunta").attr("disabled", true);
            var preguntaActual = parseInt($("#positionPregunta").val());

            var preguntaNext = parseInt(preguntaActual + 1);
            var idContainerOld = "#containerPregunta" + preguntaActual;
            var idContainerNext = "#containerPregunta" + preguntaNext;

            $(idContainerOld).hide();
            $(idContainerNext).show();

            var calcularPorcentaje = (preguntaNext * 100) / cantpreguntasTotal;
           // var calcularPorcentaje = (preguntaNext * 100) / 40;

            if (preguntaNext == cantpreguntasTotal) {
           // if (preguntaNext == 40) {
                var stringBarCode = '<div class="progress progress-lg"><div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"><span class="sr-only">100% Complete</span></div></div>';
                $("#contentBarIdLoading").empty();
                $("#contentBarIdLoading").html(stringBarCode);
            } else {
                var stringBarCode = '<div class="progress progress-lg"><div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" aria-valuenow="' + calcularPorcentaje + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + calcularPorcentaje + '%;"><span class="sr-only">' + calcularPorcentaje + '% Complete</span></div></div>';
                $("#contentBarIdLoading").empty();
                $("#contentBarIdLoading").html(stringBarCode);
            }

            $("#positionPregunta").val(preguntaNext);
        }
    </script>



    <script>
            function validateForm(){

                var faltan  = 0 ;
                var marcados  = 0 ;
                for (var i = 0; i < 40; i++) {
                  //console.log(i);
                  var urlData =  "radio" + (i+1);
                  //console.log(urlData);
                  var data =   $('input[name='+urlData+']:checked').val();

                  if (data == undefined) {
                    faltan = faltan + 1;
                    //console.log("no selecciono");
                  }else{
                    marcados = marcados + 1 ;
                    //console.log(data);
                  }

                }

                $("#countDataNotFound").text(faltan);
                //console.log("Faltan : " + faltan);
                $("#countDataFound").text(marcados);
                //console.log("Marcados : " + marcados);

                if (faltan==0) {
                    $('#myModal2').modal('show');
                }else{
                    $('#myModal').modal('show');
                }

                    return false;
            }


    </script>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel"><font color="#FF0000"><strong>¡ IMPORTANTE !</strong></font></h4>
                </div>
                <div class="modal-body">
                    <div class="icon info" style="display: block;"></div>
                    <h3><p style="text-align: justify;">Existen preguntas las cuales no ha seleccionado, por favor revisar y seleccionar su alternativa.</p></h3>
                    <br>
                    <!-- <p class="lead text-muted" style="display: block;">Existen preguntas las cuales no ha seleccionado, por favor revisar y seleccionar su alternartiva.</p>Modal -->

                    <div class="alert alert-danger">Preguntas sin resolver &nbsp;: &nbsp;<strong id="countDataNotFound">10</strong></div>
                    <div class="alert alert-success">Preguntas resueltas   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;<strong id="countDataFound">20</strong></div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
   <!-- < <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">¡  Aviso !</h4>
                </div>
                <div class="modal-body">
                    <div class="icon info" style="display: block;"></div>
                    <h2>¿ Está seguro que desea terminar el simulacro ?</h2>
                    <p class="lead text-muted" style="display: block;">Al enviar sus preguntas no podrá cambiar las alternativas seleccionadas.</p>
                </div>
                <div id="loadingSimulacro" style="display:none">
                    <center>
                        <h3>Procesando...</h3>
                        <img src="./MTC _ Simulacro de Examen de Conocimientos_files/loading.gif" width="50%" alt="">
                    </center>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button id="buttomLoadingFinish" onclick="showLoading()" type="button" class="btn btn-primary">Terminar Simulacro</button>
                </div>
            </div>
        </div>
    </div>

 -->
    <!-- Modal -->
    <div class="modal fade" id="myModal30" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">¡  Aviso !</h4>
                </div>
                <div class="modal-body">
                    <div class="icon info" style="display: block;"></div>
                    <h2>Selecciona una alternativa</h2>
                    <center>
                        <img src="./MTC _ Simulacro de Examen de Conocimientos_files/loading.gif" width="50%" alt="">
                    </center>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>



    <script>
            function timeOutSimu(){
                document.formularioSimulacro.submit();
            }
            function  showLoading(){
                $("#buttomLoadingFinish").prop('disabled', true);
                $("#loadingSimulacro").show();
                document.formularioSimulacro.submit();
            }
    </script>



    <script type="text/javascript" charset="utf-8">
        $(document).keydown(function(tecla){
            if (tecla.keyCode == 65) {
                $("input:radio:visible").each(function () {
                    var data = $(this).val();
                    if (data == "A") {
                        $(this).click();
                        $("#btnSiguientePregunta").attr("disabled", false);
                        var preguntaActual = parseInt($("#positionPregunta").val());
                        // alert(preguntaActual);
                        //alert(preguntaActual);
                        if (preguntaActual == cantpreguntasTotal) {

                            //if (preguntaActual == 40) {
                            $("#btnSiguientePregunta").hide();
                            $("#btnFinishPregunta").show();
                        }

                    }
                });
            }else if(tecla.keyCode == 66) {
                $("input:radio:visible").each(function () {
                    var data = $(this).val();
                    if (data == "B") {
                        $(this).click();
                        $("#btnSiguientePregunta").attr("disabled", false);
                        var preguntaActual = parseInt($("#positionPregunta").val());
                        // alert(preguntaActual);
                        //alert(preguntaActual);
                        if (preguntaActual == cantpreguntasTotal) {

                            //if (preguntaActual == 40) {
                            $("#btnSiguientePregunta").hide();
                            $("#btnFinishPregunta").show();
                        }

                    }
                });
            }else if(tecla.keyCode == 67){
                $("input:radio:visible").each(function () {
                    var data = $(this).val();
                    if (data == "C") {
                        $(this).click();
                        $("#btnSiguientePregunta").attr("disabled", false);
                        var preguntaActual = parseInt($("#positionPregunta").val());
                        // alert(preguntaActual);
                        //alert(preguntaActual);
                        if (preguntaActual == cantpreguntasTotal) {

                            //if (preguntaActual == 40) {
                            $("#btnSiguientePregunta").hide();
                            $("#btnFinishPregunta").show();
                        }

                    }
                });
            }else if (tecla.keyCode == 68) {
                $("input:radio:visible").each(function () {
                    var data = $(this).val();
                    if (data == "D") {
                        $(this).click();
                        $("#btnSiguientePregunta").attr("disabled", false);
                        var preguntaActual = parseInt($("#positionPregunta").val());
                        // alert(preguntaActual);
                        //alert(preguntaActual);
                        if (preguntaActual == cantpreguntasTotal) {

                            //if (preguntaActual == 40) {
                            $("#btnSiguientePregunta").hide();
                            $("#btnFinishPregunta").show();
                        }

                    }
                });
            } else if (tecla.keyCode == 13) {
                if ($('#btnSiguientePregunta').is(':disabled')) {
                    //alert("OK TODO ");
                    console.log("Ingrese una alternativa ");
                    // $('#myModal30').modal('show');
                } else {
                    var cantidad = $("#positionPregunta").val();
                    if (cantidad == 40) {
                        var dataProceso = $("#checkout2").val();
                        if (dataProceso == 1) {
                            $('#myModal2').modal('show');
                            $("#checkout2").val(2);
                        }else {
                            $("#buttomLoadingFinish").click();
                        }
                    } else {
                        nextPregunta();
                    }
                }
            }
        });
    </script>


    @endsection
