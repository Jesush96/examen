<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <meta name="description" content="Simulacro de Examen de Conocimientos en la conduccion para postulantes a Licencias de conducir - MTC">
          <meta name="author" content="DGTT">
          <meta http-equiv="Cache-Control" content="no-cache">
          <!-- Favicons -->
          <link rel="apple-touch-icon" sizes="57x57" href="https://sierdgtt.mtc.gob.pe/Content/img/apple-icon-57x57.png">
          <link rel="apple-touch-icon" sizes="60x60" href="https://sierdgtt.mtc.gob.pe/Content/img/apple-icon-60x60.png">
          <link rel="apple-touch-icon" sizes="72x72" href="https://sierdgtt.mtc.gob.pe/Content/img/apple-icon-72x72.png">
          <link rel="apple-touch-icon" sizes="76x76" href="https://sierdgtt.mtc.gob.pe/Content/img/apple-icon-76x76.png">
          <link rel="apple-touch-icon" sizes="114x114" href="https://sierdgtt.mtc.gob.pe/Content/img/apple-icon-114x114.png">
          <link rel="apple-touch-icon" sizes="120x120" href="https://sierdgtt.mtc.gob.pe/Content/img/apple-icon-120x120.png">
          <link rel="apple-touch-icon" sizes="144x144" href="https://sierdgtt.mtc.gob.pe/Content/img/apple-icon-144x144.png">
          <link rel="apple-touch-icon" sizes="152x152" href="https://sierdgtt.mtc.gob.pe/Content/img/apple-icon-152x152.png">
          <link rel="apple-touch-icon" sizes="180x180" href="https://sierdgtt.mtc.gob.pe/Content/img/apple-icon-180x180.png">
          <link rel="icon" type="image/png" sizes="192x192" href="https://sierdgtt.mtc.gob.pe/Content/img/android-icon-192x192.png">
          <link rel="icon" type="image/png" sizes="32x32" href="https://sierdgtt.mtc.gob.pe/Content/img/favicon-32x32.png">
          <link rel="icon" type="image/png" sizes="96x96" href="https://sierdgtt.mtc.gob.pe/Content/img/favicon-96x96.png">
          <link rel="icon" type="image/png" sizes="16x16" href="https://sierdgtt.mtc.gob.pe/Content/img/favicon-16x16.png">
          <link rel="manifest" href="https://sierdgtt.mtc.gob.pe/Content/img/manifest.json">
          <meta name="msapplication-TileColor" content="#ffffff">
          <meta name="msapplication-TileImage" content="/Content/img/ms-icon-144x144.png">
          <meta name="theme-color" content="#ffffff">
          <title>MTC | Simulacro de Examen de Conocimientos</title>
          <!-- Fonts -->
          <link rel="stylesheet" src="{{ asset('css/font-awesome.min.css') }}">
          <!-- Styles -->
          <link href="{{ asset('css/app.css') }}" rel="stylesheet">
          <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

          <link href="{{ asset('css/core.css') }}" rel="stylesheet" type="text/css" />
          <link href="{{ asset('css/components.css') }}" rel="stylesheet" type="text/css" />
          <link href="{{ asset('css/icons.css') }}" rel="stylesheet" type="text/css" />
          <link href="{{ asset('css/pages.css') }}" rel="stylesheet" type="text/css" />
          <link href="{{ asset('css/menu.css') }}" rel="stylesheet" type="text/css" />
          <link href="{{ asset('css/responsive.css') }}" rel="stylesheet" type="text/css" />
          <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
          <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
          <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
          <![endif]-->
         <!-- --------------------------------- -->

          <style>
              .contadorSimulacro {
                  font-size: 40px;
                  color: #d4393d;
                  font-family: 'Rancho', cursive;
              }

              .titleContandorSimulacro {
                  font-size: 40px;
                  color: #d4393d;
                  font-family: 'Rancho', cursive;
              }
          </style>
          <script language="JavaScript">
              //////////F12 disable code////////////////////////
                  document.onkeypress = function (event) {
                      event = (event || window.event);
                      if (event.keyCode == 123) {
                         //alert('No F-12');
                          return false;
                      }
                  }
                  document.onmousedown = function (event) {
                      event = (event || window.event);
                      if (event.keyCode == 123) {
                          //alert('No F-keys');
                          return false;
                      }
                  }
              document.onkeydown = function (event) {
                      event = (event || window.event);
                      if (event.keyCode == 123) {
                          //alert('No F-keys');
                          return false;
                      }
                  }
              /////////////////////end///////////////////////
          </script>
          <script language="Javascript">
              document.oncontextmenu = function(){return false}
          </script>







    </head>

    <body>
      <div class="con">
          @yield('content')
      </div>
      <!-- jQuery  -->

      <script src="{{ asset('js/app.js') }}" defer></script>
       <script src="{{ asset('js/bootstrap.min.js') }}"></script>
       <script src="{{ asset('js/detect.js') }}"></script>
       <script src="{{ asset('js/fastclick.js') }}"></script>
       <script src="{{ asset('js/jquery.slimscroll.js') }}"></script>
       <script src="{{ asset('js/jquery.blockUI.js') }}"></script>
       <script src="{{ asset('js/waves.js') }}"></script>
       <script src="{{ asset('js/wow.min.js.descarga') }}"></script>
       <script src="{{ asset('js/jquery.nicescroll.js') }}"></script>
       <script src="{{ asset('js/jquery.scrollTo.min.js') }}"></script>
       <!-- App js -->
       <script src="{{ asset('js/jquery.core.js') }}"></script>
       <script src="{{ asset('js/jquery.app.js') }}"></script>
      <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body> -->
</html>
